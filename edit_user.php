<?php
	#
	# Vorhandene Session starten
	#
	session_start();
	#
	# Ist der Nutzer eingelogged?
	#
	if($_SESSION['user_id']==0) {
?>
			<html>
			<header>
				<title>Keine Zugangsberechtigung</title>
			</header>
			<body>
				<img src="/images/studentenverzeichnis.png">
				<center><font size="10">Keine Zugangsberechtigung</font></center>
				<br><br>
				<center>Sie müssen sich einloggen einloggen oder einen Account anlegen!</center><br>
				<center><a href="/einloggen.php">Einloggen</a> - <a href="/anmelden.php">Anmelden</a></center>
				<br><br>
				</body>
			</html>		
<?php
		exit();
	}
	#
	# Wurde das Formular abgebrochen?
	#
	if($_GET['submit']=="Abbrechen") {
		header("Location: /edit_user.php");
		exit();
	}
	#
	# Neues PDO Objekt für die Datenbankverbindung erzeugen
	#
	$pdo = new PDO('mysql:host=127.0.0.1;dbname=student_db', 'studentdb', 'stud1pass');
	#
	# SQL Query um Nutzeraten aus der Datenbank zu laden
	#
	$get_student_by_id=$pdo->prepare("SELECT * FROM students as s left join semester_students as sem on (s.id=sem.student_id) where s.id=".$_SESSION['user_id']);
	$get_student_by_id->execute();
	$user = $get_student_by_id->fetchAll();
	#
	# Daten aus dem HTTP Request
	#
	$new_student_id=-1;	
	$email=$_GET["email"] ?: $user[0]['email'];
	$password=$_GET["password"] ?: $user[0]['password'];
	$firstname=$_GET["firstname"] ?: $user[0]['firstname'];
	$lastname=$_GET["lastname"] ?: $user[0]['lastname'];
	$street=$_GET["street"] ?: $user[0]['street'];
	$zip=$_GET["zip"] ?: $user[0]['zip'];
	$city=$_GET["city"] ?: $user[0]['city'];
	$semester=$_GET["semester"] ?: $user[0]['semester_id'];
	#
	# Wurde das Formular schon  ausgefüllt und abgeschickt?
	#
	if($_GET["submit"] != "") { 
		$submited=true;
	} else {
		$submited=false;
	}
	#
	# Falls alle Daten übergeben wurden dann die Datenbankeinträge aktualisieren
	#
	if(!($email == "" || $password == "" || $firstname == "" || $street  == "" || $zip  == "" || $city  == "" || $semester  == "")  && $submited == true ) {
		#
		# Daten des Studenten aktuallisieren
		#
		$statement = $pdo->prepare("UPDATE students SET email=?, firstname=?, lastname=?, street=?, zip=?, city=?, password=? where id=?");
		$statement->execute(array($email, $firstname, $lastname, $street, $zip, $city, $password,$_SESSION['user_id']));
		# 
		# Semester des Studenten aktuallisieren
		#
		$statement = $pdo->prepare("UPDATE semester_students SET semester_id=? where student_id=?");
		$statement->execute(array($semester,$_SESSION['user_id']));
?>			
<html>
	<header>
		<title>Userdata is updated!</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Userdata is updated!</font></center>
		<br><br>
		<center><a href="/students.php">Studenten Übersicht</a> - <a href="/edit_user.php">Eigene Daten ändern</a> -  <a href="/ausloggen.php">Ausloggenn</a> </center>
	</body>
</html>	
<?php	
		exit();
	}
?>
<html>
	<header>
		<title>Eigenen Benutzer editieren</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Eigenen Benutzer Editieren</font></center>
		<br><br>
		<form action="/edit_user.php" method="get">
		Emailadresse: <input type=text name=email length=30 value="<?php echo $email; ?>"></input>
		<font color="red">
		<?php if($email == "" && $submited) { echo "Email Adresse fehlt!"; }; ?>
		<?php if($new_student_id == 0 && $submited) { echo "Account mit dieser Emailadresse existiert schon!"; }; ?>
		</font>
		<br>
		Passwort: <input type=password name=password length=30 value="<?php echo $password; ?>"></input>
		<font color="red">
		<?php if($password == "" && $submited) { echo "Email Passwort fehlt!"; } ?>
		</font>
		<br>
		Vorname: <input type=text name=firstname length=30 value="<?php echo $firstname; ?>"></input>
		<font color="red">
		<?php if($firstname == "" && $submited) { echo "Vorname fehlt!"; };  ?>
		</font>
		<br>
		Nachname: <input type=text name=lastname  length=30 value="<?php echo $lastname; ?>"></input>
		<font color="red">
		<?php if($lastname == "" && $submited) { echo "Nachname fehlt!"; }; ?>
		</font>
		<br>
		Strasse: <input type=text name=street length=30 value="<?php echo $street; ?>"></input>
		<font color="red">
		<?php if($street == "" && $submited) { echo "Strasse fehlt!"; }; ?>
		</font>
		<br>
		Postleitzahl: <input type=text name=zip length=5 value="<?php echo $zip; ?>"></input> Stadt: <input type=text name=city length=30 value="<?php echo $city; ?>"></input>
		<font color="red">
		<?php if($zip == "" && $submited) { echo "Postleitzahl fehlt!"; }; ?>
		&nbsp;
		<?php if($city == "" && $submited) { echo "Stadt fehlt!"; }; ?>
		</font>
		<br>
		Semester: <select name=semester>
			<option value="">Bitte auswählen</option>
		<?php
		#
		# Mögliche Semster aus der Datenbank laden
		#
		$get_semester_query="SELECT id,name FROM semesters";
		foreach ($pdo->query($get_semester_query) as $row) {
			if($semester==$row['id']) {
				$option_select_string=" selected";
			} else {
				$option_select_string="";
			}	
			echo "<option value=\"".$row['id']."\"".$option_select_string.">".$row['name']."</option>\n";
		}
		?>
		</select>
		<font color="red">
		<?php if($semester == "" && $submited) { echo "Bitte Semester wählen!"; }; ?>
		</font>
		<br>	
		<input type=submit name="submit" value="Abschicken"> 	<input type=submit name="submit" value="Abbrechen">
		</form>
		<br><br>
		<center><a href="/students.php">Studenten Übersicht</a> - <a href="/edit_user.php">Eigene Daten ändern</a> -  <a href="/ausloggen.php">Ausloggen</a> </center>
	</body>
</html>
