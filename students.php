<?php
	#
	# Vorhandene Session starten
	#	
	session_start();
	#
	# Ist der Nutzer eingelogged?
	#
	if($_SESSION['user_id']>0) {
		#
		# Neues PDO Objekt für die Datenbankverbindung erzeugen
		#
		$pdo = new PDO('mysql:host=127.0.0.1;dbname=student_db', 'studentdb', 'stud1pass');
		#
		# SQL Query um Nutzeraten aus der Datenbank zu laden
		#
		$get_allstudents_query="SELECT * FROM students AS s LEFT JOIN semester_students AS sems ON (s.id=sems.student_id) LEFT JOIN semesters AS sem ON (sems.semester_id = sem.id) ORDER BY sem.name,s.lastname,s.firstname";		
?>
<html>
<header>
	<title>Students</title>
</header>
<body>
	<img src="/images/studentenverzeichnis.png">
	<center><font size="10">Studenten</font></center>
	<br><br>
	<table border=1>
		<tr>
		<th>Semester</th><th>Vorname</th><th>Nachname</th><th>Strasse</th><th>PLZ</th><th>Ort</th>
		</tr>	
<?php
	#
	# Nutzerdaten aus der Datenbank laden
	#
	$count=0;
	foreach ($pdo->query($get_allstudents_query) as $row) {
		echo "<tr><td>".$row['name']."</td><td>".$row['firstname']."</td><td>".$row['lastname']."</td><td>".$row['street']."</td><td>".$row['zip']."</td><td>".$row['city']."</td><tr>";
		$count++;
	}
	if($count==0) {
		echo "<tr colspan=6><td>Keine Studenten Vorhanden</td><tr>";	
	}
?>	
	</table>
	<br><br>
	<center><a href="/students.php">Studenten Übersicht</a> - <a href="/edit_user.php">Eigene Daten ändern</a> -  <a href="/ausloggen.php">Ausloggen</a> </center>
	</body>
</html>
<?php
	} else {
?>
		<html>
		<header>
			<title>Keine Zugangsberechtigung</title>
		</header>
		<body>
			<img src="/images/studentenverzeichnis.png">
			<center><font size="10">Keine Zugangsberechtigung</font></center>
			<br><br>
			<center>Sie müssen sich einloggen einloggen oder einen Account anlegen!</center><br>
			<center><a href="/einloggen.php">Einloggen</a> - <a href="/anmelden.php">Anmelden</a></center>
			<br><br>
			</body>
		</html>		
<?php
	}
?>